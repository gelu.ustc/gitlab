import { initSimpleApp } from '~/helpers/init_simple_app_helper';
import PlanLimitsApp from './components/plan_limits_app.vue';

initSimpleApp('#js-plan-limits-app', PlanLimitsApp);
